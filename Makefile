# -*- Makefile -*-

install: install-binary install-upstream

install-binary:
	[[ -x /usr/local/bin/bb ]] || pip install -e .

install-upstream:
	@if ! git remote show -n | grep -q upstream; then \
		echo "Adding remote upstream"; \
		git remote add upstream https://bitbucket.org/zhemao/bitbucket-cli; \
	fi

uninstall:
	pip uninstall --yes bitbucket-cli
	rm -f /usr/local/bin/bb
	rm -f /usr/local/bin/bitbucket


